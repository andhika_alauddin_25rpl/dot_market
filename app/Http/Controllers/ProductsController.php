<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Products;
use Session;

class ProductsController extends Controller
{
    public function index()
    {
        $paginate=5;
        $products = Products::paginate(5);
        $counter = 1;

        if (request()->has('page') && request()->get('page') > 1) {
            $counter += (request()->get('page') - 1) * $paginate;
        }

        return view ('products.products', compact('products','counter'));
    }

    public function detail($id)
    {
        $p = Products::find($id);
        return view('products.detail_prod', compact('p'));
    }

    public function edit($id)
    {
        $p = Products::find($id);
        return view('products.edit', compact('p'));
    }

    public function store(Request $request)
    {

        $validation= $request->validate([
            'name' => 'required',
            'category' => 'required',
            'price' => 'required|numeric'
        ]);
        $product = new Products;

        $product->name = $request->name;
        $product->category = $request->category;
        $product->unit_price = $request->price;

        $save = $product->save();
        return redirect()->back();
        if ($save) {
            Session::flash('message', 'Berhasil tambah');
            return redirect()->back();
        }
        else {
            return redirect()->back();
        }
    }

    public function update(Request $request, $id)
    {
        $validation= $request->validate([
            'name' => 'required',
            'category' => 'required',
            'price' => 'required|numeric'
        ]);

        $product = Products::find($id);
        $product->name = $request->name;
        $product->category = $request->category;
        $product->unit_price = $request->price;

        $save = $product->save();
        if ($save) {
            Session::flash('message', 'Berhasil edit');
            return redirect('/product');
        }
        else {
            return redirect('/product');
        }
    }

    public function delete($id)
    {
        $del = Products::find($id);

        $del->delete();
        return redirect()->back();
    }
}
