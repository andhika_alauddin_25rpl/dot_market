<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;

class kategoriCon extends Controller
{
    public function index(Request $request)
    {
        $data = Session::get('kategori');
        return view('template', ['kategori' => $data]);

    }

    public function submit(Request $request)
    {
        if(!Session::has('kategori')){
            Session::put('kategori',
            array( $request->kategori ));
        }
        else{
            Session::push('kategori',
            array( $request->kategori ));
        }

        return redirect('/kategori');
    }

    public function deleteSession(Request $request)
    {
        Session::forget('kategori');
        return redirect('/kategori');        
    }
}
