<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class loginController extends Controller
{
    public function index()
    {
        if (Auth::user()) {
            return redirect('/product');
        }
        else {
            return view('auth.login');            
        }
    }

    public function doLogin(Request $request)
    {
        $credentials = $request->except('_token');

        if (Auth::attempt($credentials)) {
            return redirect('/product');
        }
        else {
            return redirect()->back();
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/login');
    }
}
