<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class homeCon extends Controller
{
    //Function tampilan awal controller
    public function index()
    {
        return view('welcome');
    }

    //Function halaman isi dengan parameter string
    public function isi($isi)
    {
        return "Halo, Saya $isi";
    }
}
