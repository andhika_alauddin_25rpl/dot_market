@include('base.header')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Products
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <!-- form start -->
            @if (Session::has('message'))
          <div class="alert alert-success">{{Session::get('message')}}</div>
            @endif
            <form role="form" action="/product/add" method="POST">
              <div class="box-body">
                <div class="form-group @if ($errors->has('name')) has-error @endif">
                  <label for="exampleInputEmail1">Nama Produk</label>
                  <input type="text" name="name" class="form-control" placeholder="Masukkan nama produk" value="{{old('name')}}">
                  <span class="help-block">{{$errors->first('name')}}</span>
              </div>
                <div class="form-group @if ($errors->has('category')) has-error @endif">
                  <label for="exampleInputEmail1">Kategori Produk</label>
                  <input type="text" name="category" class="form-control" placeholder="Masukkan kategori produk" value="{{old('category')}}">
                  <span class="help-block">{{$errors->first('category')}}</span>
                </div>
                <div class="form-group @if ($errors->has('price')) has-error @endif">
                  <label for="exampleInputEmail1">Harga Produk</label>
                  <input type="text" name="price" class="form-control" placeholder="Masukkan Harga Produk" value="{{old('price')}}">
                  <span class="help-block">{{$errors->first('price')}}</span>
                </div>
            </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <input type="submit" value="Submit" name="Submit" class="btn btn-success">
            </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Daftar Produk</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered">
                <tr>
                    <th>No</th>
                  <th>Nama Produk</th>
                  <th>Kategori</th>
                  <th>Harga</th>
                  <th>Action</th>
                </tr>
                @if ($products != NULL)
                @foreach ($products as $p)
                <tr>
                    <td>{{$counter++}}</td>
                    <td>{{$p->name}}</td>
                    <td>{{$p->category}}</td>
                    <td>Rp {{number_format($p->unit_price)}}</td>
                    <td><a href="/product/{{$p->id_products}}" class="btn btn-primary"><span class="glyphicon glyphicon-list-alt"></span></a>
                        <a href="/product/{{$p->id_products}}/edit" class="btn btn-warning"><span class="glyphicon glyphicon-edit"></span></a>
                        <a href="/product/{{$p->id_products}}/delete" class="btn btn-danger" onclick="return confirm('Apakah anda ingin menghapus data ini?')"><span class="glyphicon glyphicon-trash"></span></a>    
                    </td>
                  </tr>                      
                @endforeach                    
                @endif
              </table>
            </div>
            <div class="text-center">
                {!! $products ->appends(request()->all())->links() !!}
            </div>
          </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
@include('base.footer')