@include('base.header')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Detail Products
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <!-- form start -->
              <div class="box-body">
              <table>
                <tr class="form-group">
                  <td>Nama Produk</td>
                  <td>:</td>
                  <td>{{$p->name}}</td>
                </tr>
                <tr class="form-group">
                  <td>Kategori</td>
                  <td>:</td>
                  <td>{{$p->category}}</td>
                </tr>
                <tr class="form-group">
                  <td>Harga</td>
                  <td>:</td>
                  <td>{{$p->unit_price}}</td>  
                </tr>
              </table><br>
            <a href="{{'/product'}}" class="btn btn-primary"><span class="glyphicon glyphicon-home"></span> HOME</a>
            <a href="/product/{{$p->id_products}}/edit" class="btn btn-warning"><span class="glyphicon glyphicon-edit"></span> EDIT</a>
            </div>
              <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
@include('base.footer')