@include('base.header')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Products
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <a href="{{'/product'}}" class="btn btn-primary"><span class="glyphicon glyphicon-home"></span></a>
            <!-- form start -->
          <form role="form" action="/product/{{$p->id_products}}/update" method="POST">
              <div class="box-body">
                    <input type="hidden" name="id" value="{{$p->id_products}}">@csrf
                    <div class="form-group @if ($errors->has('name')) has-error @endif">
                        <label for="exampleInputEmail1">Nama Produk</label>
                        <input type="text" name="name" class="form-control" placeholder="Masukkan nama produk" value="{{$p->name}}">
                        <span class="help-block">{{$errors->first('name')}}</span>
                    </div>
                    <div class="form-group @if ($errors->has('category')) has-error @endif">
                        <label for="exampleInputEmail1">Kategori Produk</label>
                        <input type="text" name="category" class="form-control" placeholder="Masukkan kategori produk" value="{{$p->category}}">
                        <span class="help-block">{{$errors->first('category')}}</span>
                    </div>
                    <div class="form-group @if ($errors->has('price')) has-error @endif">
                        <label for="exampleInputEmail1">Harga Produk</label>
                        <input type="text" name="price" class="form-control" placeholder="Masukkan Harga Produk" value="{{$p->unit_price}}">
                        <span class="help-block">{{$errors->first('price')}}</span>
                    </div>
                      </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <input type="submit" value="Submit" name="Submit" class="btn btn-success">
            </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
@include('base.footer')