<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route halaman login
Route::get('/login', 'loginController@index')->name('login');
//Route proses login
Route::post('/login/do', 'loginController@doLogin');
//Route Logout
Route::get('/logout', 'loginController@logout');
Route::middleware('auth')->group(function(){
//Route halaman awal web
Route::get('/', function () {
    return view('template',['kategori' => []]);
});

//Route halaman awal Kategori
Route::get('/kategori', 'kategoriCon@index');
//Route submit form kategori
Route::post('/kategori','kategoriCon@submit');
//Route delete Session
Route::get('/kategori/delete', 'kategoriCon@deleteSession');

//Route index product
Route::get('/product', 'ProductsController@index');
//Route detail product
Route::get('/product/{id}','ProductsController@detail');
//Route edit product
Route::get('/product/{id}/edit','ProductsController@edit');
//Route tambah data product
Route::post('/product/add', 'ProductsController@store');
//Route update data product
Route::post('/product/{id}/update', 'ProductsController@update');
//Route delete data product
Route::get('/product/{id}/delete', 'ProductsController@delete');

//Route index product
Route::get('/customer', 'CustomerController@index');
//Route detail product
Route::get('/customer/{id}','CustomerController@detail');
//Route edit product
Route::get('/customer/{id}/edit','CustomerController@edit');
//Route tambah data product
Route::post('/customer/add', 'CustomerController@store');
//Route update data product
Route::post('/customer/{id}/update', 'CustomerController@update');
//Route delete data product
Route::get('/customer/{id}/delete', 'CustomerController@delete');
});